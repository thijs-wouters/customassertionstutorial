package custom.assertions.tutorial;

public enum Gender {
    MALE,
    FEMALE,
    OTHER,
}
