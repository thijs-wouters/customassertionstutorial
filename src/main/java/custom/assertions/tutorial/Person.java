package custom.assertions.tutorial;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class Person {
    private String name;
    private LocalDate dateOfBirth;
    private int age;
    private Gender gender;
    private Collection<Person> children = Collections.emptyList();

    public Person(String name, LocalDate dateOfBirth, int age, Gender gender, Collection<Person> children) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.gender = gender;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public Collection<Person> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", age=" + age +
                ", gender=" + gender +
                ", children=" + children +
                '}';
    }

    public static final class PersonBuilder {
        private String name;
        private LocalDate dateOfBirth;
        private int age;
        private Gender gender;
        private Collection<Person> children = Collections.emptyList();

        private PersonBuilder() {
        }

        public static PersonBuilder aPerson() {
            return new PersonBuilder();
        }

        public PersonBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder withDateOfBirth(LocalDate dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public PersonBuilder withAge(int age) {
            this.age = age;
            return this;
        }

        public PersonBuilder withGender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public PersonBuilder withChildren(PersonBuilder... children) {
            this.children = Arrays.stream(children)
                    .map(PersonBuilder::build)
                    .collect(Collectors.toList());
            return this;
        }

        public PersonBuilder withChild(PersonBuilder child) {
            return withChildren(child);
        }

        public Person build() {
            return new Person(name, dateOfBirth, age, gender, children);
        }
    }
}
