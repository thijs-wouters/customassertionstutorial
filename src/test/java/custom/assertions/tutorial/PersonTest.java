package custom.assertions.tutorial;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Objects;

import static custom.assertions.tutorial.Person.PersonBuilder.aPerson;
import static custom.assertions.tutorial.PersonTest.MyCustomAssertions.assertThat;

public class PersonTest {
    private Person mark;

    @Before
    public void setUp() {
        mark = aPerson()
                .withName("Mark")
                .withAge(37)
                .withDateOfBirth(LocalDate.of(1982, Month.DECEMBER, 23))
                .withGender(Gender.MALE)
                .withChild(aPerson()
                        .withName("Daisy")
                        .withAge(9)
                        .withGender(Gender.FEMALE)
                        .withDateOfBirth(LocalDate.of(2011, Month.APRIL, 10)))
                .build();
    }

    @Test
    public void theOldWay() {
        Assertions.assertThat(mark.getName()).isEqualTo("Mark");
    }

    @Test
    public void withConditions() {
        Assertions.assertThat(mark).has(name("Mark"));
        Assertions.assertThat(mark).doesNotHave(name("Daisy"));
        Assertions.assertThat(mark).is(named("Mark"));
        Assertions.assertThat(mark).isNot(named("Daisy"));
    }

    private Condition<Person> name(String name) {
        return new Condition<>(person -> Objects.equals(person.getName(), name), String.format("name %s", name));
    }

    private Condition<Person> named(String name) {
        return new Condition<>(person -> Objects.equals(person.getName(), name), String.format("named %s", name));
    }

    @Test
    public void conditionsCanBeUsedForCollections() {
        Assertions.assertThat(mark.getChildren()).are(named("Daisy"));
        Assertions.assertThat(mark.getChildren()).have(name("Daisy"));
        Assertions.assertThat(mark.getChildren()).areAtLeast(1, named("Daisy"));
    }

    @Test
    public void customAssertions() {
        PersonAssert.assertThat(mark)
                .hasName("Mark")
                .isNamed("Mark")
                .hasChild(named("Daisy"));
    }

    static class PersonAssert extends AbstractAssert<PersonAssert, Person> {
        private PersonAssert(Person person) {
            super(person, PersonAssert.class);
        }

        public static PersonAssert assertThat(Person actual) {
            return new PersonAssert(actual);
        }

        public PersonAssert isNamed(String name) {
            return assertName(name, "Expected person to be named <%s>, but was named <%s>.");
        }

        public PersonAssert hasName(String name) {
            return assertName(name, "Expected person name to be <%s>, but was <%s>.");
        }

        private PersonAssert assertName(String name, String errorMessage) {
            isNotNull();

            if (!actual.getName().equals(name)) {
                failWithMessage(errorMessage, name, actual.getName());
            }

            return this;
        }

        public PersonAssert hasChild(Condition<Person> childCondition) {
            isNotNull();
            Assertions.assertThat(actual.getChildren()).areAtLeastOne(childCondition);
            return this;
        }
    }

    @Test
    public void magicHappens() {
        assertThat(mark)
                .hasChild(named("Daisy"))
                .isNamed("Mark");
        assertThat(true)
                .overridingErrorMessage("Boolean logic broke")
                .isTrue();
    }

    public static class MyCustomAssertions extends Assertions {
        public static PersonAssert assertThat(Person actual) {
            return PersonAssert.assertThat(actual);
        }
    }
}
